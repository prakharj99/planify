import { Component, OnInit } from '@angular/core';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { TaskService } from '../task.service';

@Component({
  selector: 'app-task-manager',
  templateUrl: './task-manager.component.html',
  styleUrls: ['./task-manager.component.scss']
})
export class TaskManagerComponent implements OnInit {

  title: any = '';
  description: any = '';
  priority: any = '';

  pending = [
    { title: 'Call Dad', description: 'call to Dad', priority: 'high' },
    { title: 'Go Shopping', description: 'shopping', priority: 'low' }
  ];

  inProcess = [
    { title: 'Homework', description: 'Homework', priority: 'high' }
  ];

  completed = [
    { title: 'Clean Room', description: 'cleaning', priority: 'medium' }
  ];
  editFlag: boolean;
  type: any;

  constructor(private taskService: TaskService) { }

  ngOnInit() {
    this.pending = this.taskService.getPendingList() ?
      this.taskService.getPendingList() :
      this.pending;
    this.inProcess = this.taskService.getInProcessList() ?
      this.taskService.getInProcessList() :
      this.inProcess;
    this.completed = this.taskService.getCompletedList() ?
      this.taskService.getCompletedList() :
      this.completed;
  }

  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
    }
    localStorage.setItem('pendingInfo', JSON.stringify(this.pending));
    localStorage.setItem('inProcessInfo', JSON.stringify(this.inProcess));
    localStorage.setItem('completedInfo', JSON.stringify(this.completed));
  }

  save() {
    this.pending.push({ title: this.title, description: this.description, priority: this.priority });
    localStorage.setItem('pendingInfo', JSON.stringify(this.pending));
    this.clearModal();
  }

  clearModal() {
    this.editFlag = false;
    this.title = '';
    this.description = '';
    this.priority = '';
  }

  delete(data, type) {
    if (type == 'pending') {
      let i = this.pending.indexOf(data);
      this.pending.splice(i, 1);
      localStorage.setItem('pendingInfo', JSON.stringify(this.pending));
    } else if (type == 'inProcess') {
      let i = this.inProcess.indexOf(data);
      this.inProcess.splice(i, 1);
      localStorage.setItem('inProcessInfo', JSON.stringify(this.inProcess));
    } else {
      let i = this.completed.indexOf(data);
      this.completed.splice(i, 1);
      localStorage.setItem('completedInfo', JSON.stringify(this.completed));
    }
    this.clearModal();
  }

  edit(data, type) {
    this.editFlag = true;
    this.delete(data, type);
    this.title = data.title;
    this.description = data.description;
    this.priority = data.priority;
    this.type = type;
  }

  update() {
    if (this.type == 'pending') {
      this.pending.push({ title: this.title, description: this.description, priority: this.priority });
      localStorage.setItem('pendingInfo', JSON.stringify(this.pending));
    } else if (this.type == 'inProcess') {
      this.inProcess.push({ title: this.title, description: this.description, priority: this.priority });
      localStorage.setItem('inProcessInfo', JSON.stringify(this.inProcess));
    } else {
      this.completed.push({ title: this.title, description: this.description, priority: this.priority });
      localStorage.setItem('completedInfo', JSON.stringify(this.completed));
    }
    this.clearModal();
  }
}
