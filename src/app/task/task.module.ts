import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from "@angular/forms";
import { HttpClientModule } from '@angular/common/http';
import { TaskRoutingModule } from './task-routing.module';
import { TaskManagerComponent } from './task-manager/task-manager.component';
import { ScrollDispatchModule } from '@angular/cdk/scrolling';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { TaskService } from './task.service';

@NgModule({
  declarations: [TaskManagerComponent],
  imports: [
    CommonModule,
    TaskRoutingModule,
    ScrollDispatchModule,
    DragDropModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [TaskService]
})

export class TaskModule { }
