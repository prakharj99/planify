import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  constructor() { }

  getPendingList() {
    return JSON.parse(localStorage.getItem('pendingInfo'));
  }

  getInProcessList() {
    return JSON.parse(localStorage.getItem('inProcessInfo'));
  }

  getCompletedList() {
    return JSON.parse(localStorage.getItem('completedInfo'));
  }
}
